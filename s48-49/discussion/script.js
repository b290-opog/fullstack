let posts = [];
let count = 1;

document.querySelector("#form-add-post").addEventListener("submit", e => {
    e.preventDefault();

    posts.push({
            id : count,
            title : document.querySelector("#txt-title").value,
            body : document.querySelector("#txt-body").value
        });
    count++;

    showPost(posts);
    alert(`succesfully added`);

    document.querySelector("#txt-title").value = null;
    document.querySelector("#txt-body").value = null;
});

const showPost = (posts) => {
    let postEntries = "";
    // To iterate each posts from the API
    posts.forEach((post) => {
      postEntries += `
      <div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title}</h3>
        <p id="post-body-${post.id}">${post.body}</p>
        <button onclick="editPost('${post.id}')">Edit</button>
        <button onclick="deletePost('${post.id}')">Delete</button>
      </div>
      `;
    });
    // To check what is stored in the postEntries variables.
    // console.log(postEntries);
    // To assign the value of postEntries to the element with "div-post-entries" id.
    document.querySelector("#div-post-entries").innerHTML = postEntries;
};



// EDIT POST DATA / EDIT BUTTON

const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;
  
    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
  
    document.querySelector("#btn-submit-update").removeAttribute("disabled");
  };

  document.querySelector("#form-edit-post").addEventListener("submit", e =>{
    e.preventDefault();

     for(let i = 0; i < posts.length; i++){
        if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
            posts[i].title = document.querySelector("#txt-edit-title").value;
            posts[i].body = document.querySelector("#txt-edit-body").value;

            showPost(posts);
            alert("Successfully updated");
            break;

        }
    } 
    /*
        let id = document.querySelector("#txt-edit-id").value;
        posts[id - 1].title = document.querySelector("#txt-edit-title").value;
        posts[id - 1].body = document.querySelector("#txt-edit-body").value;
        showPost(posts);
    */
})




/* ACADEMIC BREAK */

const deletePost = id => {
    const post = document.querySelector(`#post-${id}`);
    
    if(post){
        let postIndex = null;

        for(let post of posts){
            if(id == post.id){
                postIndex = posts.indexOf(post);
                break;
            };
        };

        if(postIndex !== null){
            posts.splice(postIndex, 1);
        };

        post.remove();
        
    };
};