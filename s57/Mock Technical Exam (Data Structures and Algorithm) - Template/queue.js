let collection = [];

// Write the queue functions below.
function print(){
    return collection
}

function enqueue(item){
    collection[collection.length] = item;
    return collection
}

function dequeue(item){
    for(let i = 0; i <collection.length -1; i++){
        collection[i] = collection[i + 1];
    }
    collection.length--;
    return collection;
}

function front(){
    return collection[0];
}

function size(){
    return collection.length;
}

function isEmpty(){
    return collection.length === 0;
}


module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};