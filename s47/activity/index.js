const txtfirstName = document.querySelector("#txt-first-name");
const txtlastName = document.querySelector("#txt-last-name");
const spanfullName = document.querySelector("#span-full-name");

txtfirstName.addEventListener("keyup" ,printFullName );

txtlastName.addEventListener("keyup", printFullName);

function printFullName(){
    spanfullName.innerHTML = `${txtfirstName.value} ${txtlastName.value}`;
}

